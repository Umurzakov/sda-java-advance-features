package ee.sda.javaadvancefeatures.datastructures;

import java.util.Arrays;

public class List {

    // 1, 4, 3, 8, 5, 77
    // 0, 1, 2, 3, 4, 5

    // Size of list
    private int size = 0;

    // How many elements we can keep by default in list
    private static final int DEFAULT_CAPACITY = 10;

    // This array will store all elements added to the list
    private int elements[];

    // List list = new List();
    public List() {
        elements = new int[DEFAULT_CAPACITY];
    }

    // List list = new List(100);
    public List(int limit) {
        elements = new int[limit];
    }

    // CRUD - Create(Adding), Read, Update or Delete

    public void add(int newItem){
        // If there is no space in array
        if (size == elements.length){
            ensureCapacity();
        }

        elements[size++] = newItem;
    }

    public int remove(int i) {
        if(i >= size || i < 0 ){
            System.out.println("Index out of bound or you are giving index negative");
        }

        // Please delete an element at index 1
        // 3, 2, 4 -> 3, 4

        int deletedItem = elements[i];
        int numElements = elements.length - (i + 1);

        // [2..8)
        System.arraycopy(elements, i + 1, elements, i, numElements);

        size--;
        return deletedItem;
    }

    private void ensureCapacity() {
        int newSize = elements.length * 2;
        // Please practice with Arrays class from Java
        elements = Arrays.copyOf(elements, newSize);

        // 3, 2, 4 - arr1;
        // 3, 2, 4, null, null, null - arr2
    }


    //TODO 3: Add some useful methods for list
    // Homework to do:
    //  Read
    //  Print
    //  Update

    //Read
    public int get(int index){

//        if(index >= size || index < 0){
//            System.out.println("Sorry, we do not have this element in list");
//        }
        return elements[index];
    }
}

