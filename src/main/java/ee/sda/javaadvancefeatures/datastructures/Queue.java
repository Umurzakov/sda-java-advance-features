package ee.sda.javaadvancefeatures.datastructures;

public class Queue {
    // 2, 1, 4, 7, 5
    // Gets element only from the ending
    // 7, 2, 1, 4, 7, 5
    // Puts an element to the beginning
    // FIFO
    // Put an element to the end
    // Get an element from the beginning.

    private static int front, end, capacity;
    private static int queue[];

    public Queue(int c) {
        front = end = 0;
        capacity = c;
        queue = new int[capacity];
    }

    // Insert element to the end of queue
    public void enqueue(int newItem){
        if(capacity == end){
            System.out.println("Sorry, queue is full");
            return;
        } else {
            queue[end] = newItem;
            end++;
        }
    }

    // remove an element from the front
    public void dequeue(){
        // If queue is empty
        if(front == end){
            System.out.println("Sorry, queue is empty");
            return;
        } else {
            // 0, 2, 1, 4, 8, 5

            // 7, 2, 1, 4, 8, 5
            for (int i = 0; i < end - 1; i++) {
                queue[i] = queue[i + 1];
                //2,1,4,8,5,7
            }

            // 2,1,4,8,5,0
            if(end < capacity){
                queue[end] = 0;
            }

            end--;
        }
    }

    public void display(){
        // When queue is empty
        if(front == end){
            System.out.println("Sorry, queue is empty");
        }

        System.out.println();
        System.out.println("Elements are:");
        for (int i = 0; i < end; i++) {
            System.out.print(queue[i] + " ");
        }
    }

}
