package ee.sda.javaadvancefeatures.datastructures;

import java.util.Arrays;

public class Stack {
    // LIFO
    // 2, 1, 4, 7, 5
    // 2, 1, 4, 7, 5

    // Example, push 8 to the stack's end
    // 2, 1, 4, 7, 5, 8
    // Pop an element from the end again
    // 2, 1, 4, 7, 5

    // push and pop

    private int size = 0;
    private static final int DEFAULT_CAPACITY = 10;

    private int elements[];

    public Stack() {
        elements = new int[DEFAULT_CAPACITY];
    }

    public Stack(int capacity) {
        elements = new int[capacity];
    }

    //TODO 2: Set initial elements
//    public Stack(int capacity, int[] initialElements) {
//        elements = new int[capacity];
//    }

    public void push(int newItem){
        if(size == elements.length){
            ensureCapacity();
        }

        elements[size++] = newItem;
    }

    public int pop(){
        int itemToBeRemoved = elements[--size];
        elements[size] = 0;// zero means empty or no element
        return itemToBeRemoved;
    }

    private void ensureCapacity() {
        int newSize = elements.length * 2;
        elements = Arrays.copyOf(elements, newSize);
    }

    //TODO 1: Print some values for the stack
}
