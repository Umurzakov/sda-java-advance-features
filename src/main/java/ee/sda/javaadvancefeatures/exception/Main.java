package ee.sda.javaadvancefeatures.exception;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> names = new ArrayList<String>();

        names.add("John");
        names.add("Jessica");

        try {
            System.out.println(names.get(3));
        } catch (IndexOutOfBoundsException exception){
            System.out.println("There is no element on this index");
        }


        System.out.println("Code is continue working");

        // Null pointer exception
        String name = "CHECK";

        try {
            System.out.println(name.toLowerCase());
        } catch (NullPointerException exception){
            System.out.println("Crazy, name was null");

            // Logs for the later check
        } finally {
            name = "Smith";
        }

        System.out.println(name);

        // throw, throws

        try {
            dividingByZero(15, 0);
        } catch (ArithmeticException exception){
            System.out.println("Please change b not to be 0");
        }

    }

    public static void dividingByZero(int a, int b) throws ArithmeticException {
        if(b == 0) {
            throw new ArithmeticException();
        }

        System.out.println(a/b);
    }
}
