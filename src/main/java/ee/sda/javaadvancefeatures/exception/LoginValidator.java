package ee.sda.javaadvancefeatures.exception;

import java.util.ArrayList;
import java.util.List;

public class LoginValidator {

    private static List<User> users = new ArrayList<User>();

    public static boolean validate(String login,
                                   String password) throws UserNotFoundException{
        for (User user : users){
            if(user.getLogin().equals(login)
            && user.getPassword().equals(password)){
                return true;
            }
        }

        throw new UserNotFoundException("User not found in our system");
        // You can not write any code
    }

    public static void add(User user){
        users.add(user);
    }

    public static void remove(User user){
        users.remove(user);
    }

    public static List<User> getAllUsers() {
        return users;
    }
}
