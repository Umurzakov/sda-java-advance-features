package ee.sda.javaadvancefeatures.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NewStream {

    public static void main(String[] args) {

        List<Integer> integerList = new ArrayList<>();

        integerList.add(3);
        integerList.add(4);
        integerList.add(14);
        integerList.add(20);
        integerList.add(5);


        Stream<Integer> integerStream = Stream.of(3, 4, 14, 20, 5);

        // Conversion from list to Stream
        Stream<Integer> fromListToStream = integerList.stream();

//        long count = fromListToStream.count(); // termination operator

        List<Integer> integerListFromStream = fromListToStream
                .filter(i -> i > 3 && i < 14)
                .collect(Collectors.toList());

        Stream<String> stringStream = Stream.of("bob", "John", "Alisa", "bob");
        stringStream
                .filter(string -> string.equals("bob"))// intermediate
                .findAny();// termination

        // map(), BOB, JOHN, ALISA

        Stream<String> stringStream2 = Stream.of("bob", "John", "Alisa", "bob");
        List<String> allItemsNowCapitalLetters = stringStream2
                .map(string -> string.toUpperCase())
                .collect(Collectors.toList());

        System.out.println(allItemsNowCapitalLetters);

        // filter, map are intermediate methods
        // collect,count, findAny or findFirst are termination methods

        // Another intermediate operator or method like sorted()
        Stream<String> stringStream3 = Stream.of("bob", "john", "Alisa", "bob");

        List<String> sortedStrings = stringStream3
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        System.out.println(sortedStrings);
    }
}
