package ee.sda.javaadvancefeatures.stream;

import java.util.ArrayList;
import java.util.List;

public class Main {

    // Java 8, current newest version is Java 15
    // most companies still using Java 8 and Java 11

    // Stream in Java 8

    // Collections like List, Stack, Queue, Map

    public static void main(String[] args) {

        List<Integer> integerList = new ArrayList<>();

        integerList.add(3);
        integerList.add(4);
        integerList.add(14);
        integerList.add(20);
        integerList.add(5);

        System.out.println("The result of sum by using for loop: ");
        int sum = sumOfElements(integerList);

        System.out.println(sum);

        int sumWithStream = sumOfElementsWithStream(integerList);

        System.out.println("The result of sum by using stream in Java 8 : ");

        System.out.println(sumWithStream);
    }

    // 3, 4, 14, 20, 5 ...

    // 3, 4, 14 and 20,5
    // 14 and 20
    // 34
    private static int sumOfElements(List<Integer> integerList){

        int sum = 0;
        // External iteration
        for (Integer integer:integerList) {
            //starts
            if(integer > 10){
                sum += integer;
            }
            // ends
        }
        return sum;
    }

    // Stream API or Stream library
    private static int sumOfElementsWithStream(List<Integer> integerList){
        // "->" it is an operator from Java 8 called Arrow operation
        // On the left side of arrow operator, you put the name of variable
        // On the right side of arrow operator, you put the condition for the given variable
        // num -> num > 10 this called Arrow function.

        return integerList
                .stream() // initial operation
                // Method is helpful for doing some operations on Collection
                .mapToInt(num -> num) // Intermediate operation
                .filter(num -> num > 10) // Intermediate operation
                .sum(); // Termination operation
    }
}
