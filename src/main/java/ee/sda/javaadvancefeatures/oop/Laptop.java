package ee.sda.javaadvancefeatures.oop;

public class Laptop extends Computer{

    //....
    public Laptop(String cpu,
                  String ram) {
        super(cpu, ram);
    }

    @Override
    public void configure() {
        // It makes configuration the same as computer
        super.configure();
       // Laptop can have different config
        System.out.println("Laptop own config");
    }
}
