package ee.sda.javaadvancefeatures.oop;

public class Computer {

    private String cpu;
    private String ram;
    private String gpu;

    public Computer(String cpu,
                    String ram) {
        this.cpu = cpu;
        this.ram = ram;
    }

    public void configure(){
        // Does some kind of config
        // ...
        System.out.println("Computer is making config");
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }
}
