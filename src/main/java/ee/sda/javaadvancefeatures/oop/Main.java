package ee.sda.javaadvancefeatures.oop;

public class Main {

    public static void main(String[] args) {
        Computer computer =
                new Computer("Intel Core i7", "4GB");

        computer.setGpu("2GB");

        Laptop laptop = new Laptop("Intel Core i8", "16GB");

        laptop.configure();

    }
}
