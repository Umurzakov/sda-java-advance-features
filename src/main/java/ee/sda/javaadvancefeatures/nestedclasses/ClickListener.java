package ee.sda.javaadvancefeatures.nestedclasses;

// Anonymous class
public interface ClickListener {

    void onClick();
}
