package ee.sda.javaadvancefeatures.nestedclasses;

public class SubmitButtonClick implements ClickListener {
    @Override
    public void onClick() {
        System.out.println("Button clicked");
    }
}
