package ee.sda.javaadvancefeatures.nestedclasses;

public class Main {

    public static void main(String[] args) {

        // Class can contain another classes - Nested classes
        // Nested classes have 2 types: one static and inner classes

//        CPU cpu = new CPU(250.99);
//        CPU cpu2 = new CPU(250.99);
//        CPU cpu3 = new CPU(250.99);
//        CPU cpu5 = new CPU(250.99);
//
//        CPU.Processor processor = cpu.new Processor();
//
//        CPU.RAM ram = cpu.new RAM();
//
//        System.out.println("Processor Cache: " +processor.getCache());
//        System.out.println("Ram Clock speed: " +ram.getClockSpeed());
//        System.out.println("Cache price: " +processor.getCachePrice());

//        CPU cpu1 = new CPU(250.99);
//        CPU cpu2 = new CPU(300.99);
//        CPU cpu3 = new CPU(200.99);

        User newUser = new User();

        newUser.setFirstName("John", new Validator() {
            @Override
            public boolean validate(String input) {
                return !input.isEmpty()
                        && Character.isUpperCase(input.charAt(0));
            }
        });

        newUser.setPassword("!anything", new Validator() {
            @Override
            public boolean validate(String input) {
                return input.contains("!");
            }
        });

        System.out.println(newUser.getFirstName());

        // TODO:  Learn some built in methods from String class and Character class in Java
    }
}
