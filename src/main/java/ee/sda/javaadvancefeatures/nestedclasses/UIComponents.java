package ee.sda.javaadvancefeatures.nestedclasses;

public class UIComponents {

    void showComponents() {

//        SubmitButtonClick submitButtonClickSimple = new SubmitButtonClick();
//        submitButtonClickSimple.onClick();

        ClickListener submitButtonClickAnonymous =
                new ClickListener() {
                    @Override
                    public void onClick() {
                        System.out.println("Button clicked");
                    }
                };

        submitButtonClickAnonymous.onClick();

    }
}
