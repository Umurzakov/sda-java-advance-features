package ee.sda.javaadvancefeatures.nestedclasses;

public class CPU {
    // Member of outer class
    private static double price;

    public final static double SOME_CONSTANT = 4.5;

    public CPU(double price) {
        this.price = price;
    }

    // Inner or non-static classes
     static class Processor{
        // Members of inner class
        static double cores;
        static String manufacturer;

        static double getCache(){
            return 5.2;
        }

        static double getCachePrice(){
            return getCache() * price;
        }
    }

    class RAM {
        double memory;
        String manufacturer;

        double getClockSpeed(){
            return 6.0;
        }
    }
}
