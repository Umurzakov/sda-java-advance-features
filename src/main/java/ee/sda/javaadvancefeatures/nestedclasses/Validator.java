package ee.sda.javaadvancefeatures.nestedclasses;

public interface Validator {

    boolean validate(String input);
}
