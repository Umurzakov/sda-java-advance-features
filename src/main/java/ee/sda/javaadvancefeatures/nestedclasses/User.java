package ee.sda.javaadvancefeatures.nestedclasses;

public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String login;
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName, Validator validator) {

        if(validator.validate(firstName)) {
            this.firstName = firstName;
        } else {
            System.out.println("Please give correct first name");
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password, Validator validator) {
        if(validator.validate(password)) {
            this.password = password;
        } else {
            System.out.println("Sorry, please think another password");
        }
    }

    //TODO: Please implement the rest of validators

}
