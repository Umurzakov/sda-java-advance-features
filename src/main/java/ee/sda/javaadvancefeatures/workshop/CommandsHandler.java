package ee.sda.javaadvancefeatures.workshop;

public class CommandsHandler {

    private CommandEnum commandEnum;
    private Product product;

    // new CommandsHandler();
    public CommandsHandler(CommandEnum commandEnum,
                           Product product) {
        this.commandEnum = commandEnum;
        this.product = product;
    }

    public CommandEnum getCommandEnum() {
        return commandEnum;
    }

    public void setCommandEnum(CommandEnum commandEnum) {
        this.commandEnum = commandEnum;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
