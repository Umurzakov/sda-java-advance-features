package ee.sda.javaadvancefeatures.workshop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandsReader {

    // Key - I,U,D
    // Value - Insert, Update
    private static final Map<Character, CommandEnum> converter = new HashMap<>();

    static {
        // You can write any code or logic
        converter.put('I', CommandEnum.INSERT);
        converter.put('U', CommandEnum.UPDATE);
        converter.put('D', CommandEnum.DELETE);
    }

    private String filePath;

    // new CommandsReader("Path");
    // new CommandsReader("Path");
    // new CommandsReader("Path");
    // new CommandsReader("Path");
    public CommandsReader(String filePath) {
        this.filePath = filePath;
//        converter.put('I', CommandEnum.INSERT);
//        converter.put('U', CommandEnum.UPDATE);
//        converter.put('D', CommandEnum.DELETE);
    }

    public List<CommandsHandler> readFile(){
        List<CommandsHandler> handlers = new ArrayList<>();

        try(BufferedReader bufferedReader = new BufferedReader(new
                FileReader(filePath))){

            String line = "";
            while ((line = bufferedReader.readLine()) != null){
                CommandsHandler commandsHandler = processEachLine(line);

                handlers.add(commandsHandler);
            }
        }
        catch (IOException exception){
            System.out.println("Sorry, we can not open your file");
        }

        return handlers;
    }

    private CommandsHandler processEachLine(String line){
        // I:1;table;40.25
        String[] split = line.split(":");
        // split: split[0] = 'I', split[1] = '1;table;40.25'
        // Now commandEnum is INSERT
        CommandEnum commandEnum = converter.get(split[0].charAt(0));

        // split: productItems[0] = "1";
        // productItems[1] = "table";
        // productItems[2] = "40.25";
        String[] productItems = split[1].split(";");

        int id = Integer.parseInt(productItems[0]);
        String name = productItems[1];
        BigDecimal price = new BigDecimal(productItems[2]);

        CommandsHandler commandsHandler =
                new CommandsHandler(commandEnum, new Product(id, name, price));

        return commandsHandler;
    }
}
