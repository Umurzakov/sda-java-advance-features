package ee.sda.javaadvancefeatures.workshop;

import java.math.BigDecimal;
import java.util.List;

public interface IProductManagement {

    void insertProduct(Product product);

    void deleteProduct(Product product);

    void updateProduct(Product product);

    List<Product> getAllProducts();

    // Some methods...

    void getProductsByPrice(BigDecimal price);
}
