package ee.sda.javaadvancefeatures.workshop;

import java.util.List;
import java.util.Map;

public class Warehouse {

    private String name;
    private String address;
    // Composition
    private Map<Integer, Product> productListMap;

    public Warehouse(String name, String address, Map<Integer, Product> productListMap) {
        this.name = name;
        this.address = address;
        this.productListMap = productListMap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<Integer, Product> getProductListMap() {
        return productListMap;
    }

    public void setProductListMap(Map<Integer, Product> productListMap) {
        this.productListMap = productListMap;
    }
}
