package ee.sda.javaadvancefeatures.workshop;

import java.math.BigDecimal;
import java.util.*;

public class ProductManagementImpl implements IProductManagement{


    private Warehouse warehouse;
    private Map<Integer, Product> productListMap;

    // 1, 2, 3
    public ProductManagementImpl(Warehouse warehouse) {
        this.warehouse = warehouse;
        this.productListMap = new HashMap<>();
        this.warehouse.setProductListMap(productListMap);
    }

    @Override
    public void insertProduct(Product product) {
        productListMap.put(product.getId(), product);
    }

    @Override
    public void deleteProduct(Product product) {
        productListMap.remove(product.getId());
    }

    @Override
    public void updateProduct(Product product) {

        productListMap.put(product.getId(), product);
//        Product productFound = null;
//        for (Product search:productList) {
//            if(search.getId() == product.getId()){
////                productList.add(product.getId(), product);
//                productFound = search;
//            }
//        }
//
//        if(productFound != null) {
//            productFound.setName(product.getName());
//            productFound.setPrice(product.getPrice());
//        }
//
//        productList.add(productFound);
    }

    @Override
    public List<Product> getAllProducts() {
        return new ArrayList<>(productListMap.values());
    }

    @Override
    public void getProductsByPrice(BigDecimal price) {

    }
}
