package ee.sda.javaadvancefeatures.workshop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String filePath = "C:\\Users\\bumurzokov\\IdeaProjects\\sda-java-advance-features\\src\\main\\java\\ee\\sda\\javaadvancefeatures\\workshop\\commands.txt";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Do you want me to start the process?(Y/N)");

        String yesOrNo = scanner.next();

        if(yesOrNo.equalsIgnoreCase("Y")){
            // I will do all the operations
            processUserWarehouse();
        } else {
            System.out.println("Ok, see you again!");
        }

    }

    private static void processUserWarehouse(){
        Warehouse warehouse =
                new Warehouse("Apple warehouse", "Somewhere in the North", new HashMap<>());

        ProductManagementImpl management = new ProductManagementImpl(warehouse);

        CommandsReader commandsReader = new CommandsReader(filePath);

        List<CommandsHandler> commandsHandlers = commandsReader.readFile();

        for (CommandsHandler handler: commandsHandlers) {
            Product product = handler.getProduct();

            switch (handler.getCommandEnum()){
                case INSERT:
                    management.insertProduct(product);
                    break;
                case DELETE:
                    management.deleteProduct(product);
                    break;
                case UPDATE:
                    management.updateProduct(product);
                    break;
                default:
                    break;
            }
        }

        System.out.println("Your warehouse is ready! And here your all products");

        List<Product> products = management.getAllProducts();

        products
                .stream()
                .forEach(product ->
                        System.out.printf("%d: %s costs USD %7.2f%n",
                                product.getId(),
                                product.getName(),
                                product.getPrice()));
    }
}
