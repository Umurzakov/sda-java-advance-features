package ee.sda.javaadvancefeatures.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetCollection {

    // 14, 10, 45, 43, 14 -> list

    // 14, 10, 45, 43 -> set, it is not allowed in SET

    // 14 -> 4365454353, "14"

    public static void main(String[] args) {

        List<Integer> integerList = new ArrayList<Integer>();

        integerList.add(14);
        integerList.add(10);
        integerList.add(45);
        integerList.add(43);
        integerList.add(14);

//        integerList.get(1);

        System.out.println(integerList);

        Set<Integer> integerSet = new HashSet<Integer>();

        integerSet.add(14);
        integerSet.add(10);
        integerSet.add(45);
        integerSet.add(43);
        integerSet.add(14);

        System.out.println(integerSet);

        for (Integer i:integerSet){
            if(i == 45) {
                System.out.println(i);
            }
        }



//        integerSet.get(1);

        // "apple" -> 56474534543543, "apple"
    }
}
