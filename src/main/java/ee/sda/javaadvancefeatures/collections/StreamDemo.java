package ee.sda.javaadvancefeatures.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamDemo {

    public static void main(String[] args) {

        Video video1 =
                new Video(1, "Got1", "youtube.com/got.com", Video.VideoType.EPISODE);
        Video video2 =
                new Video(2, "Got2", "youtube.com/got2.com", Video.VideoType.CLIP);

        // Two option of filling the list
        List<Video> videoList = new ArrayList<>();
        videoList.add(video1);
        videoList.add(video2);

        Episode episode1 = new Episode("got1 and got2", 1, videoList);

        List<Episode> episodes = Arrays.asList(episode1);

        Season season = new Season("Season number 1", 1, episodes);

        List<Episode> episodesFromSeason = season.getEpisodeList();

        // Find an episode with name "got1 and got2"
        Optional<Episode> episodeResult = episodesFromSeason
                .stream()
                .filter(episode -> episode.getEpisodeName().equals("got1 and got2"))
                .findAny();

//        if(episodeResult != null){
//            System.out.println(episodeResult.getEpisodeName());
//        }

        // Optional from Java 8
        // The first way
        if(episodeResult.isPresent()){
            System.out.println(episodeResult.get().getEpisodeName());
        }

        // The second way of checking if episode exists
        episodeResult
                .ifPresent(episode -> System.out.println(episodeResult.get().getEpisodeName()));

        Optional<Video> foundVideo = searchVideoByGivenTitle(season, "Got1");

        System.out.println("Your video is found: ");

        foundVideo.ifPresent(video -> System.out.println(video.getTitle()));

//        if(foundVideo != null) {
//            System.out.println(foundVideo.getTitle());
//        }

        // Print all videos
        System.out.println("All videos we have: ");
        printAllVideos(season);
    }

    private static Optional<Video> searchVideoByGivenTitle(Season season,
                                                 String title){
//        for (Episode episode:season.getEpisodeList()) {
//            for (Video video:episode.getVideos()) {
//                if(video.getTitle().equals(title)){
//
//                }
//            }
//        }

        // Season -> episodes -> videos
        return season.getEpisodeList()
                .stream()
                .flatMap(episode -> episode.getVideos().stream())
                .filter(video -> video.getTitle().equals(title))
                .findFirst();

//        if(videoOptional.isPresent()){
//            return videoOptional.get();
//        }
//        return null;
//        return videoOptional;
    }

    private static void printAllVideos(Season season) {
        List<Video> videos = season.getEpisodeList()
                .stream()
                .flatMap(episode -> episode.getVideos().stream())
                .collect(Collectors.toList());

        videos.stream()
                .forEach(video -> System.out.println(video.getTitle()));


//        for (Video video:videos) {
//            System.out.println(video.getTitle());
//        }

//        videos.stream()
//                .forEach(video -> {
//                    System.out.println(video.getTitle());
//                    video.setTitle("random");
//                });

        // We skipped
        // 1. Multithreading
        // 2. Generics
    }
}
