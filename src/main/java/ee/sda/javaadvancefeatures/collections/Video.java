package ee.sda.javaadvancefeatures.collections;

public class Video {

    private int id;
    private String title;
    private String url;
    private VideoType videoType;
    // CLIP, PREVIEW, EPISODE

    public Video(int id,
                 String title,
                 String url,
                 VideoType videoType) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.videoType = videoType;
    }

    public enum VideoType {
        CLIP,
        PREVIEW,
        EPISODE
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
