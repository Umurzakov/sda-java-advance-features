package ee.sda.javaadvancefeatures.collections;

import java.util.List;


// Season:
//     episode1, episode2:
//         video1, video2
public class Season {
    private String seasonName;
    private int seasonNumber;
    private List<Episode> episodeList;

    public Season(String seasonName,
                  int seasonNumber,
                  List<Episode> episodeList) {
        this.seasonName = seasonName;
        this.seasonNumber = seasonNumber;
        this.episodeList = episodeList;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public List<Episode> getEpisodeList() {
        return episodeList;
    }

    public void setEpisodeList(List<Episode> episodeList) {
        this.episodeList = episodeList;
    }
}
