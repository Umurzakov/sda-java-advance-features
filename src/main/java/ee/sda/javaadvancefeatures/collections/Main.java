package ee.sda.javaadvancefeatures.collections;

import java.util.ArrayList;
import java.util.Arrays;
//TODO: Please check these two libraries
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        //int(primitive types) -> Integer(wrapper types)
        // double -> Double

//        Integer integer = 78;
//        int n = integer.intValue();

//        int[] arr = new int[10];
//        int[] arr2 = {1,3,2};

        List<String> vowels = new ArrayList<String>();

        //add example
        vowels.add("A");
        vowels.add("I");

        // Let's insert E between A and I
        vowels.add(1, "E");

        // Adding one list into another list
        List<String> consonants = new ArrayList<String>();
        consonants.add("B");
        consonants.add("D");

        vowels.addAll(consonants);

        System.out.println(vowels);

        String B = vowels.get(3);

        vowels.remove("B");
        vowels.remove(1);

//        List<Integer> integers = new ArrayList<Integer>();


        // printing with loop
        for (String s: vowels){
            System.out.println(s + " ");
        }

        // Covert from list to array
        String[] stringsArray = new String[vowels.size()];

        stringsArray = vowels.toArray(stringsArray);

        // Convert from array to list
        List<String> vowelsAfterConversion = Arrays.asList(stringsArray);

        // Yet another way of conversion from array to list
        List<String> vowelsNew = new ArrayList<String>();

        for (String s: stringsArray){
            vowelsNew.add(s);
        }

        // Sorting a list of elements
        Collections.sort(vowels);

        System.out.println("Sorted list in order: ");
        System.out.println(vowels);

        Collections.reverse(vowels);

        System.out.println("Sorted list in reverse order: ");
        System.out.println(vowels);

        // demo of seasonal film
        demoSeasonalFilm();
    }

    public static void demoSeasonalFilm(){
        Video video1 =
                new Video(1, "Got1", "youtube.com/got.com", Video.VideoType.EPISODE);
        Video video2 =
                new Video(2, "Got2", "youtube.com/got2.com", Video.VideoType.CLIP);

        // Two option of filling the list
        List<Video> videoList = new ArrayList<Video>();
        videoList.add(video1);
        videoList.add(video2);

        Episode episode1 = new Episode("got1 and got2", 1, videoList);

        List<Episode> episodes = Arrays.asList(episode1);

        Season season = new Season("Season number 1", 1, episodes);

        // from the first season, please take episode 1 and open video number 2's url;

        // Find required episode
        Episode foundEpisode = null;
        for (Episode episode: season.getEpisodeList()){
            if(episode.getEpisodeNumber() == 1){
                foundEpisode = episode;
            }
        }

        // Find required video
        if(foundEpisode != null){
            List<Video> videos = foundEpisode.getVideos();

            for (Video video:videos){
                if(video.getId() == 2){
                    System.out.println("The system found your video's url: " + video.getUrl());
                }
            }
        }
    }
}
