package ee.sda.javaadvancefeatures.collections2;

import ee.sda.javaadvancefeatures.exception.LoginValidator;
import ee.sda.javaadvancefeatures.exception.User;
import ee.sda.javaadvancefeatures.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Author author1 = new Author("John", "Smith", 'M', 31);
        Author author2 = new Author("Jessica", "Albana", 'F', 28);

        List<Author> authors = new ArrayList<Author>();
        authors.add(author1);
        authors.add(author2);

        Book book1 = new Book("555","John's adventure",
                50,
                2019,
                authors,
                BookType.ACTION);

        Book book2 = new Book("777","Jessica's adventure",
                100,
                2020,
                authors,
                BookType.CRIME);

        BookService bookService = new BookService();

        bookService.add(book1);
        bookService.add(book2);

        LoginValidator.add(new User("admin", "admin@pass123"));
        LoginValidator.add(new User("user1", "user1@pass123"));


        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to our library!");
        System.out.println("You need to authenticate first to use our system");

        System.out.println("Please enter your login: ");
        String login = scanner.next();

        System.out.println("Please enter your password: ");
        String password = scanner.next();

        try {
            LoginValidator.validate(login, password);
        } catch (UserNotFoundException exception) {
            System.out.println("Your credentials are not correct, please enter another");
            return;
        }

        System.out.println("All good! Now you requests are handling");

        List<Book> onlyActionBooks = bookService.findBookByBookType(BookType.ACTION);

        System.out.println(onlyActionBooks);


        System.out.println("Before sorting: ");

        System.out.println(bookService.getAllBooks());

        List<Book> sortedBooks = bookService.sortByTitleInAscendingOrder();

        System.out.println("After sorting: ");

        System.out.println(sortedBooks);


        // add new feature
        // It should able to show me BookType -> Title
        // ACTION -> John's adventure
        // ID -> Book name

        Map<String, String> mapBooks = bookService.mapBooks();
        String johnsAdventureBookTitle = mapBooks.get("555");

        System.out.println(johnsAdventureBookTitle);
    }
}
