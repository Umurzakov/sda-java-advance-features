package ee.sda.javaadvancefeatures.collections2;

import java.util.List;

public class Book implements Comparable<Book>{

    private String id;
    private String title;
    private float price;
    private int yearOfRelease;
    private List<Author> authors;
    private BookType bookType;

    public Book(String title,
                float price,
                int yearOfRelease,
                List<Author> authors,
                BookType bookType) {
        this.title = title;
        this.price = price;
        this.yearOfRelease = yearOfRelease;
        this.authors = authors;
        this.bookType = bookType;
    }

    public Book(String id,
                String title,
                float price,
                int yearOfRelease,
                List<Author> authors,
                BookType bookType) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.yearOfRelease = yearOfRelease;
        this.authors = authors;
        this.bookType = bookType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public BookType getBookType() {
        return bookType;
    }

    public void setBookType(BookType bookType) {
        this.bookType = bookType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //Book1 = "Title1"
    // Book2 = "Title2"

    // Title1, Title2 - Ascending order
    // Title2, Title1 - Descending order
    // Book1 = Book2;

    // int can be -1, 0, 1

    // Book book = new Book("...");
    // book.compareTo(book2);
    // book.compareTo(null);
    @Override
    public int compareTo(Book book2) {
        if(this == book2){
            return 0;
        }

        if(book2 == null){
            return -1;
        }

        // String1 == String2
        // "Apple" and "Orange"
        return this.getTitle().compareTo(book2.getTitle());
    }

    // toString, it comes from Object class


    @Override
    public String toString() {
        return "{ " +title +" Book type is " + bookType + " }";
    }
}
