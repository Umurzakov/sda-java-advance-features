package ee.sda.javaadvancefeatures.collections2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookService {

    private List<Book> books = new ArrayList<Book>();

    public void add(Book newBook){
        books.add(newBook);
    }

    public void remove(Book book){
        books.remove(book);
    }

    public List<Book> getAllBooks(){
        return books;
    }

    //Find all books by book type
    // Filtering
    public List<Book> findBookByBookType(BookType bookType){
        List<Book> results = new ArrayList<Book>();

        for (Book book :books){
            if(bookType.equals(book.getBookType())){
                results.add(book);
            }
        }
        return results;
    }

    // Find a book by given year of released and after
    public List<Book> findBookByYear(int yearOfRelease){
        List<Book> results = new ArrayList<Book>();

        for (Book book :books){
            if(yearOfRelease == book.getYearOfRelease()
               || yearOfRelease > book.getYearOfRelease()){
                results.add(book);
            }
        }
        return results;
    }

    // Sort all books by title
    public List<Book> sortByTitleInAscendingOrder(){
        Collections.sort(books);
        return books;
    }

    // Map Book id to Book name
    public Map<String, String> mapBooks(){
        Map<String, String> idToBookNameMap = new HashMap<String, String>();

        for (Book book : books){
            idToBookNameMap.put(book.getId(), book.getTitle());
        }

        return idToBookNameMap;
    }
}
