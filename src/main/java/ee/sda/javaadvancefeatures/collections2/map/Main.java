package ee.sda.javaadvancefeatures.collections2.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        //Map, CRUD
        // key, value

        // ID number -> Citizen
        // email -> User
        // phone number -> Home

        List<Item> items = new ArrayList<Item>();
        items.add(new Item("Id number", "Estonian citizen"));

        // Hash functions, "Tallinn" -> "fffdd333keras"
        Map<String, String> idToCitizenMap = new HashMap<String, String>();
        idToCitizenMap.put("3905643434", "John");
        idToCitizenMap.put("4905643435", "Jessica");
        //....

        String citizen = idToCitizenMap.get("3905643434");

        System.out.println("Map consist of: ");
        System.out.println(idToCitizenMap);

        idToCitizenMap.remove("3905643434");

        System.out.println("After we remove one citizen: ");
        System.out.println(idToCitizenMap);

        // Added again removed citizen
        idToCitizenMap.put("3905643434", "John");

        // Update the name of citizen
        idToCitizenMap.put("3905643434", "John2");

        System.out.println("After we update citizen's name: ");
        System.out.println(idToCitizenMap);

        Set<String> ids = idToCitizenMap.keySet();

        System.out.println(ids);
    }
}
