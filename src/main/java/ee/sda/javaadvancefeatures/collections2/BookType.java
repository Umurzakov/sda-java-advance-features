package ee.sda.javaadvancefeatures.collections2;

public enum BookType {
    ACTION,
    FANTASY,
    CRIME
}
