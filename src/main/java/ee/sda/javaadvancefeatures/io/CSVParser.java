package ee.sda.javaadvancefeatures.io;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVParser {


    // I am using Java I/O libraries
    // java.io
    // java.nio
    public static void main(String[] args) {

        String justRandomString = "test";
        // Array of bytes
        byte[] bytes;
        // One byte
        byte oneByte = 1;
        // Stream of data

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new FileReader("C:\\Users\\bumurzokov\\IdeaProjects\\sda-java-advance-features\\src\\main\\java\\ee\\sda\\javaadvancefeatures\\io\\employees.csv"));

            parserCSVData(reader);

        } catch (FileNotFoundException e) {
            System.out.println("File not found, please specify correct path");
        } catch (IOException e) {
            System.out.println("Sorry, something went wrong while reading the file");
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                System.out.println("Sorry, something went wrong while closing the file");
            }
        }
    }

    private static void parserCSVData(BufferedReader reader) throws IOException {

        Scanner scanner = null;
        String line = "";

        List<Employee> employeeList = new ArrayList<>();

        int index = 0;
        while ((line = reader.readLine()) != null){
            scanner = new Scanner(line);
            scanner.useDelimiter(",");

            Employee newEmployee = new Employee();

            while (scanner.hasNext()){
                // Employee id
                // Employee name
                String data = scanner.next();

                if(index == 0){
                    newEmployee.setId(Integer.parseInt(data));
                } else if(index == 1){
                    newEmployee.setName(data);
                } else if(index == 2){
                    newEmployee.setRole(data);
                } else if(index == 3){
                    newEmployee.setSalary(data);
                }
                index++;
            }
            index = 0;
            employeeList.add(newEmployee);
        }

//        System.out.println(employeeList);

        for (Employee employee:employeeList){
            System.out.println(employee.getId() +"| "
                    +employee.getName() +"| "+ employee.getRole() +"| "+employee.getSalary());
        }
    }
}
