package ee.sda.javaadvancefeatures.io;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

public class DownloadCsvFile {

    public static void main(String[] args) {
        String url = "https://docs.google.com/spreadsheets/d/1QsTr-UMP5RFh2TWzV5CDhnHT4dIbcVmqv-8D2N2l-ZU/edit#gid=0";

        try {
            downloadUsingStream(url, "C:\\Users\\bumurzokov\\IdeaProjects\\sda-java-advance-features\\fileAny.csv");
        } catch (IOException e) {
            System.out.println("I need to know what happened");
        }
    }

    private static void downloadUsingStream(String urlStr, String file) throws IOException{
        URL url = new URL(urlStr);
        BufferedInputStream bis = new BufferedInputStream(url.openStream());
        FileOutputStream fis = new FileOutputStream(file);
        byte[] buffer = new byte[1024];
        int count=0;
        while((count = bis.read(buffer,0,1024)) != -1)
        {
            fis.write(buffer, 0, count);
        }
        fis.close();
        bis.close();
    }

}
